#!/bin/bash -e

set -e

# Include bash utilities
echo "Include Source remote git repo:"
source <(curl -s https://raw.githubusercontent.com/YBorodin/bash_scripts/master/output.sh)

#Input parameters
BRANCHNAME=$1

if [[ ${BRANCHNAME} =~ ^(dev|master)$ ]]; then
    print_title "Select branch: $BRANCHNAME"
else 
    print_error "The branch name is not correct. Sript was stopped!"
fi    


#Deploy part
print_info "Start deploy part..."
print_info "Unzip artefacts..."
unzip -oq *.zip

DEPLOY_FOLDER="/var/www/nodes/$BRANCHNAME"
print_title "Deploy folder: $DEPLOY_FOLDER"
mkdir -p "$DEPLOY_FOLDER"

#Config prepair
if [[ ${BRANCHNAME} =~ ^(dev)$ ]]; then
    sed -i s#%ENV%#develop#g ./ecosystem.config.js
    sed -i s#development#develop#g ./ecosystem.config.js
    export NODE_ENV=develop
    ENV="develop"
 #   print_info "BRANCH = $BRANCHNAME : ENV=develop"
elif [[ ${BRANCHNAME} =~ ^(master)$ ]]; then
    sed -i s#%ENV%#prod#g ./ecosystem.config.js
    sed -i s#development#prod#g ./ecosystem.config.js
    export NODE_ENV=prod
    ENV="prod"
 #   print_info "BRANCH = $BRANCHNAME : ENV=prod"
#elif [[ ${BRANCHNAME} =~ ^(feature/*)$ ]]; then    
#    sed -i s#%ENV%#test#g ./ecosystem.config.js
#    sed -i s#development#test#g ./ecosystem.config.js
#    export NODE_ENV=develop
#    ENV="test"
fi

#sed -i s#development#$BRANCHNAME#g ./ecosystem.config.js

print_info "Print $(pwd)"
print_info "Print ecosystem.config.js "
cat ecosystem.config.js

mkdir -p "$DEPLOY_FOLDER/config"
cp ./config/"$ENV".json "$DEPLOY_FOLDER/config/local.config.json"

print_info "Print application config file"
cat "$DEPLOY_FOLDER/config/local.config.json"

print_info "Copy application to $DEPLOY_FOLDER"
cp -r . "$DEPLOY_FOLDER"

print_info "PM2 version: $(pm2 --version)"
cd "$DEPLOY_FOLDER"
#export NODE_ENV=$BRANCHNAME
#export NODE_ENV=development
pm2 start

# Test part
print_info "Start test part"
npm run test || true
