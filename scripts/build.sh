#!/bin/bash -e

# Include bash utilities
echo "Include Source remote git repo:"
source <(curl -s https://raw.githubusercontent.com/YBorodin/bash_scripts/master/output.sh)

/bin/bash
set -e
ls -la

print_info "NODE - $(node -v)"
print_info "NPM - $(npm -v)"
print_info "YARN - $(yarn --version)"

# Linter
print_info "Start linter part..."
npm run linter || true

# Build
print_info "Start build..."
yarn install

# Test
#print_info "Start test part..."
#npm run test 

